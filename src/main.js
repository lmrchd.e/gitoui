import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

let $open_repo = window.$('#repository_open');

$open_repo.click(function () {
  alert('this should open repo');
});